from enum import Enum

from cubes_manager import *
from sudokubi import *


class SudokubiSolver:
    """
    Solves a sudokubi puzzle, in several intermediate steps.
    """

    class Type(Enum):
        ORIGINAL = 0
        RANDOM = 1

    def __init__(self, initial_sudokubi, tp) -> None:
        self.puzzle = initial_sudokubi
        if tp == SudokubiSolver.Type.ORIGINAL:
            self.cubes = OriginalSet(self.puzzle.symbol_manager)
        else:
            if tp == SudokubiSolver.Type.RANDOM:
                self.cubes = RandomSet(self.puzzle.symbol_manager)

        self.solutions = []
        self.puzzle.invariant()

    def __repr__(self):
        return f'SudokubiSolver() puzzle: {repr(self.puzzle)}'

    def __str__(self):
        return f'SudokubiSolver() puzzle: {self.puzzle}'

    def solve(self, shuffle_coordinates=False):
        puzzle = self.puzzle

        cornercoordinates = Sudokubi.corner_coordinates
        edgecoordinates = Sudokubi.edge_coordinates
        centercoordinates = Sudokubi.center_coordinates

        if shuffle_coordinates:
            shuffle_coordinates_keep_first_corner(cornercoordinates, edgecoordinates, centercoordinates)

        cornersolutions = []
        for aco in self.force_first_corner(puzzle):
            for bco in self.place_next_corner(aco, cornercoordinates[1]):
                for cco in self.place_next_corner(bco, cornercoordinates[2]):
                    for dco in self.place_next_corner(cco, cornercoordinates[3]):
                        for eco in self.place_next_corner(dco, cornercoordinates[4]):
                            for fco in self.place_next_corner(eco, cornercoordinates[5]):
                                for gco in self.place_next_corner(fco, cornercoordinates[6]):
                                    for hco in self.place_next_corner(gco, cornercoordinates[7]):
                                        cornersolutions.append(hco)

        edgesolutions = []
        for edgeindex in range(len(cornersolutions)):
            for aed in self.place_next_edge(cornersolutions[edgeindex], edgecoordinates[0]):
                for bed in self.place_next_edge(aed, edgecoordinates[1]):
                    for ced in self.place_next_edge(bed, edgecoordinates[2]):
                        for ded in self.place_next_edge(ced, edgecoordinates[3]):
                            for eed in self.place_next_edge(ded, edgecoordinates[4]):
                                for fed in self.place_next_edge(eed, edgecoordinates[5]):
                                    for ged in self.place_next_edge(fed, edgecoordinates[6]):
                                        for hed in self.place_next_edge(ged, edgecoordinates[7]):
                                            for ied in self.place_next_edge(hed, edgecoordinates[8]):
                                                for jed in self.place_next_edge(ied, edgecoordinates[9]):
                                                    for ked in self.place_next_edge(jed, edgecoordinates[10]):
                                                        for led in self.place_next_edge(ked, edgecoordinates[11]):
                                                            edgesolutions.append(led)

        centersolutions = []
        for centerindex in range(len(edgesolutions)):
            for ace in self.place_next_center(edgesolutions[centerindex], centercoordinates[0]):
                for bce in self.place_next_center(ace, centercoordinates[1]):
                    for cce in self.place_next_center(bce, centercoordinates[2]):
                        for dce in self.place_next_center(cce, centercoordinates[3]):
                            for ece in self.place_next_center(dce, centercoordinates[4]):
                                for fce in self.place_next_center(ece, centercoordinates[5]):
                                    centersolutions.append(fce)

        for solution in centersolutions:
            print(solution)

        return self.solutions

    def force_first_corner(self, puzzle):
        cubes_corner = self.cubes.cubes3[0]
        for index, cube_rotation in enumerate(cubes_corner):
            if puzzle.place_cube(0, 0, 2, cube_rotation):
                puzzle.used_corner_indices.append(0)
                puzzle.remaining_corner_indices.remove(0)
                puzzle.metadata_text_block.add_to_corner_info(str(1) + "(" + str(index) + ")")
                assert (puzzle.invariant())
        return [puzzle]

    def place_next_corner(self, puzzle, coordinates):
        assert len(coordinates) == 3
        x, y, z = coordinates

        placeholder = puzzle.get_cube_location(x, y, z)
        next_corner_matches = []

        for corneriterator in puzzle.remaining_corner_indices:
            for index in range(len(self.cubes.cubes3[corneriterator])):
                next_cube_corner = self.cubes.cubes3[corneriterator][index]

                if puzzle.is_correct_placement(next_cube_corner, placeholder):
                    copy_puzzle = copy.deepcopy(puzzle)

                    copy_puzzle.place_cube(x, y, z, next_cube_corner)
                    copy_puzzle.used_corner_indices.append(corneriterator)
                    copy_puzzle.remaining_corner_indices.remove(corneriterator)

                    copy_puzzle.metadata_text_block.add_to_corner_info(
                        str(corneriterator + 1) + "(" + str(index) + ")")
                    assert (copy_puzzle.invariant())

                    if copy_puzzle not in next_corner_matches:
                        next_corner_matches.append(copy_puzzle)

        return next_corner_matches

    def place_next_edge(self, puzzle, coordinates):
        assert len(coordinates) == 3
        x, y, z = coordinates

        placeholder = puzzle.get_cube_location(x, y, z)
        next_edge_matches = []

        for edgeiterator in puzzle.remaining_edge_indices:
            for index in range(len(self.cubes.cubes2[edgeiterator])):
                next_cube_edge = self.cubes.cubes2[edgeiterator][index]

                if puzzle.is_correct_placement(next_cube_edge, placeholder):
                    copy_puzzle = copy.deepcopy(puzzle)

                    copy_puzzle.place_cube(x, y, z, next_cube_edge)
                    copy_puzzle.used_edge_indices.append(edgeiterator)
                    copy_puzzle.remaining_edge_indices.remove(edgeiterator)

                    copy_puzzle.metadata_text_block.add_to_edge_info(str(edgeiterator + 1) + "(" + str(index) + ")")
                    # todo add metadata
                    assert (copy_puzzle.invariant())

                    if copy_puzzle not in next_edge_matches:
                        next_edge_matches.append(copy_puzzle)

        return next_edge_matches

    def place_next_center(self, puzzle, coordinates):
        assert len(coordinates) == 3
        x, y, z = coordinates

        placeholder = puzzle.get_cube_location(x, y, z)
        next_center_matches = []

        for centeriterator in puzzle.remaining_center_indices:
            for index in range(len(self.cubes.cubes1[centeriterator])):
                next_cube_center = self.cubes.cubes1[centeriterator][index]

                if puzzle.is_correct_placement(next_cube_center, placeholder):
                    copy_puzzle = copy.deepcopy(puzzle)

                    copy_puzzle.place_cube(x, y, z, next_cube_center)
                    copy_puzzle.used_center_indices.append(centeriterator)
                    copy_puzzle.remaining_center_indices.remove(centeriterator)

                    copy_puzzle.metadata_text_block.add_to_center_info(
                        str(centeriterator + 1) + "(" + str(index) + ")")
                    assert (copy_puzzle.invariant())

                    if copy_puzzle not in next_center_matches:
                        next_center_matches.append(copy_puzzle)

        return next_center_matches


def shuffle_coordinates_keep_first_corner(cornercoordinates, edgecoordinates, centercoordinates):
    original_first = cornercoordinates[0]

    random.shuffle(cornercoordinates)
    random.shuffle(edgecoordinates)
    random.shuffle(centercoordinates)

    cornercoordinates.remove(original_first)
    cornercoordinates.insert(0, original_first)
