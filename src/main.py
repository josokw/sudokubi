import argparse
import time

from sudokubi_solver import *

__version__ = "1.0.0"


def main():
    print(f'Sudokubi v{__version__}')

    parser = argparse.ArgumentParser(
        description="Sudokubi solves a 3D Sudoku",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    subparsers = parser.add_subparsers(title='R selection', help="options", dest='R')
    R_parser = subparsers.add_parser('R', help='Random options')

    parser.add_argument('-v', '--version',
                        action="store_true",
                        help="show version")

    R_parser.add_argument('-s', '--seed',
                          type=int,
                          help="set seed <int> random generator")

    R_parser.add_argument('-a', '--attempts',
                          type=int,
                          default=1,
                          help="set the number of attempts <int> >= 1")

    parser.add_argument('-m', '--metadata',
                        default=False,
                        action='store_true',
                        help='show metadata')

    args = parser.parse_args()

    show_metadata = args.metadata

    if args.version:
        print()
        print(f'Sudokubi v{__version__}')
        exit()

    if args.R == 'R':
        if args.seed:
            random.seed(args.seed)

    if args.R == 'R':
        if args.attempts < 1:
            print()
            print(f'ERROR: number of attempts should be > 0')
            exit()

    t_start = time.perf_counter()

    symb_manager = StandardSetManager()

    # execute rest of program
    if args.R == 'R':
        for attempt in range(1, args.attempts + 1):
            print(f'----- Attempt = {attempt}')
            puzzle = Sudokubi(symb_manager, show_metadata)
            solver = SudokubiSolver(puzzle, SudokubiSolver.Type.RANDOM)
            solver.solve(False)
    else:
        puzzle = Sudokubi(symb_manager, show_metadata)
        solver = SudokubiSolver(puzzle, SudokubiSolver.Type.ORIGINAL)
        solver.solve(False)

    t_end = time.perf_counter()
    print(f'Execution time = {round(t_end - t_start, 3)} sec')


if __name__ == '__main__':
    main()
