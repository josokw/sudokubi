import copy


class TextBlock:
    """
    Represents a 2-dimensional string (text block): [0][0] ... [xmax-1][ymax-1].
    """

    def __init__(self, xmax: int, ymax: int, ch=' '):
        assert len(ch) == 1, \
            f'NOT: len(ch)={len(ch)} is 1 char'
        self.ymax = ymax
        self.xmax = xmax
        self.ch = ch
        self.txt = []
        for line in range(0, ymax):
            self.txt.append(ch * xmax)

    def __repr__(self) -> str:
        return f'TextBlock(xmax={self.xmax}, ymax={self.ymax}, ch={self.ch})'

    def __str__(self) -> str:
        """ Returns text block as one string using '\n' """
        result = ''
        for line in self.txt:
            line += '\n'
            result += line
        return result

    def __add__(self, other):
        assert self.ymax == other.ymax, \
            f'NOT: ymax={self.ymax} equals other.ymax={other.ymax}'
        result = TextBlock(self.xmax + other.xmax, self.ymax)
        for index in range(0, result.ymax):
            result.txt[index] = self.txt[index] + other.txt[index]
        return result

    def __mul__(self, n: int):
        assert n > 1, \
            f'NOT: n={n} > 1'
        txtb = copy.deepcopy(self)
        result = copy.deepcopy(self)
        for add in range(1, n):
            result += txtb
        return result

    def line(self, y: int) -> str:
        return self.txt[y]

    def add(self, other, x: int, y: int):
        ln = 0
        for line in other.txt:
            if y > self.ymax:
                break
            self.write(line, x, y + ln)
            ln += 1

    def clear(self, ch=' '):
        """ Will fill all text with ch characters. """
        assert len(ch) == 1, \
            f'NOT: len(ch)={len(ch)} is 1 char'
        self.txt = []
        for line in range(0, self.ymax):
            self.txt.append(ch * self.xmax)

    def write(self, line: str, x: int, y: int):
        assert x < self.xmax and y < self.ymax, \
            f'NOT: {x} < xmax={self.xmax} and {y} < ymax={self.ymax}'
        # A string is not modifiable, typecast to list is necessary and reverse.
        txt_list = list(self.txt[y])
        for c in line:
            if x >= self.xmax:
                break
            else:
                txt_list[x] = c
                x += 1
        self.txt[y] = ''.join(txt_list)


class TextBlockBordered(TextBlock):
    """
    Specialization of TextBlock, now with border characters: + - |
    """

    def __init__(self, xmax: int, ymax: int, ch: str = ' ') -> None:
        assert xmax > 1 and ymax > 1, \
            f'NOT: xmax={xmax} > 1 and ymax={ymax} > 1'
        super().__init__(xmax, ymax, ch)
        super().write('+' + (xmax - 2) * '-' + '+', 0, 0)
        super().write('+' + (xmax - 2) * '-' + '+', 0, ymax - 1)
        for row in range(1, ymax - 1):
            super().write('|', 0, row)
            super().write('|', xmax - 1, row)

    def clear(self, ch=' '):
        self.__init__(self.xmax, self.ymax, self.ch)
