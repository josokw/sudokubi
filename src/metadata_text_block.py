from cube_text_block import *


class MetadataTextBlock(TextBlock):
    """
    Contains Sudokubi metadata info.
    """

    X_MAX = FaceTextBlock.X_MAX * 3 * 4
    Y_MAX = 4

    def __init__(self):
        super().__init__(MetadataTextBlock.X_MAX, MetadataTextBlock.Y_MAX, '.')

    def __str__(self) -> str:
        return super().__str__()

    def _update(self):  # todo implement
        pass

    def clear(self):
        self.__init__()
