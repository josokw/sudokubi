from text_block import *


class FaceTextBlock(TextBlockBordered):
    """" Face text block with borders, sized 7x7 """

    X_MAX = 7
    Y_MAX = 7

    def __init__(self, face=0, value=0, direction=0):
        super().__init__(FaceTextBlock.X_MAX, FaceTextBlock.Y_MAX)
        self.update(face, value, direction)

    def update(self, face=0, value=0, direction=0):
        super().__init__(FaceTextBlock.X_MAX, FaceTextBlock.Y_MAX)
        if face != 0:
            super().write(str(face), 0, 0)
        if direction != 0:
            super().write(str(direction), 1, 1)
        if value != 0:
            super().write(str(value), FaceTextBlock.X_MAX // 2, FaceTextBlock.Y_MAX // 2)

    @staticmethod
    def create_empty() -> object:
        return TextBlock(FaceTextBlock.X_MAX, FaceTextBlock.Y_MAX)


class CubeTextBlock(TextBlock):
    """ Contains all 6 faces """

    def __init__(self):
        super().__init__(FaceTextBlock.X_MAX * 4, FaceTextBlock.Y_MAX * 3, '.')
        # Empty
        super().add(FaceTextBlock.create_empty(), 0, 0)
        super().add(FaceTextBlock.create_empty(), FaceTextBlock.X_MAX * 2, 0)
        super().add(FaceTextBlock.create_empty(), FaceTextBlock.X_MAX * 3, 0)
        super().add(FaceTextBlock.create_empty(), 0, FaceTextBlock.Y_MAX * 2)
        super().add(FaceTextBlock.create_empty(), FaceTextBlock.X_MAX * 2, FaceTextBlock.Y_MAX * 2)
        super().add(FaceTextBlock.create_empty(), FaceTextBlock.X_MAX * 2, 0)
        super().add(FaceTextBlock.create_empty(), FaceTextBlock.X_MAX * 3, FaceTextBlock.Y_MAX * 2)
        # Faces 1...6
        super().add(FaceTextBlock(3), FaceTextBlock.X_MAX, 0)
        super().add(FaceTextBlock(5), 0, FaceTextBlock.Y_MAX)
        super().add(FaceTextBlock(1), FaceTextBlock.X_MAX, FaceTextBlock.Y_MAX)
        super().add(FaceTextBlock(2), FaceTextBlock.X_MAX * 2, FaceTextBlock.Y_MAX)
        super().add(FaceTextBlock(6), FaceTextBlock.X_MAX * 3, FaceTextBlock.Y_MAX)
        super().add(FaceTextBlock(4), FaceTextBlock.X_MAX, FaceTextBlock.Y_MAX * 2)

    def __str__(self):
        return super().__str__()

    def clear(self):
        self.__init__()
