from cube_location import *
from sudokubi_metadata import *


class Sudokubi:
    """
    Has 6 faces, every face has 9 sub-faces of corresponding CubeLocation faces.
    Contains indices matched cubes and a 3D array of CubeLocation objects.
    """

    count_str = 0
    wait_for_key_pressed = False

    corner_coordinates = [
        [0, 0, 2],
        [2, 0, 2],
        [2, 2, 2],
        [0, 2, 2],
        [2, 2, 0],
        [2, 0, 0],
        [0, 2, 0],
        [0, 0, 0]]

    edge_coordinates = [
        [1, 0, 2],
        [0, 1, 2],
        [2, 1, 2],
        [1, 2, 2],
        [0, 0, 1],
        [0, 2, 1],
        [2, 0, 1],
        [2, 2, 1],
        [1, 0, 0],
        [0, 1, 0],
        [2, 1, 0],
        [1, 2, 0]]

    center_coordinates = [
        [1, 1, 2],
        [1, 0, 1],
        [1, 2, 1],
        [2, 1, 1],
        [0, 1, 1],
        [1, 1, 0]]

    def __init__(self, symbol_manager, show_metadata=True):
        assert isinstance(symbol_manager, SymbolManager)
        self.show_metadata_text_block = show_metadata
        self.id = '1'
        self.remaining_corner_indices = [0, 1, 2, 3, 4, 5, 6, 7]
        self.used_corner_indices = []
        self.remaining_edge_indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        self.used_edge_indices = []
        self.remaining_center_indices = [0, 1, 2, 3, 4, 5]
        self.used_center_indices = []
        self.ALL_CANDIDATES = symbol_manager.symbol_symbol_list
        self.symbol_manager = symbol_manager

        self.face_candidates = [set(),
                                {symbol_manager.symbol_list[0].symbol, symbol_manager.symbol_list[1].symbol,
                                 symbol_manager.symbol_list[2].symbol, symbol_manager.symbol_list[3].symbol,
                                 symbol_manager.symbol_list[4].symbol, symbol_manager.symbol_list[5].symbol,
                                 symbol_manager.symbol_list[6].symbol, symbol_manager.symbol_list[7].symbol,
                                 symbol_manager.symbol_list[8].symbol},
                                {symbol_manager.symbol_list[0].symbol, symbol_manager.symbol_list[1].symbol,
                                 symbol_manager.symbol_list[2].symbol, symbol_manager.symbol_list[3].symbol,
                                 symbol_manager.symbol_list[4].symbol, symbol_manager.symbol_list[5].symbol,
                                 symbol_manager.symbol_list[6].symbol, symbol_manager.symbol_list[7].symbol,
                                 symbol_manager.symbol_list[8].symbol},
                                {symbol_manager.symbol_list[0].symbol, symbol_manager.symbol_list[1].symbol,
                                 symbol_manager.symbol_list[2].symbol, symbol_manager.symbol_list[3].symbol,
                                 symbol_manager.symbol_list[4].symbol, symbol_manager.symbol_list[5].symbol,
                                 symbol_manager.symbol_list[6].symbol, symbol_manager.symbol_list[7].symbol,
                                 symbol_manager.symbol_list[8].symbol},
                                {symbol_manager.symbol_list[0].symbol, symbol_manager.symbol_list[1].symbol,
                                 symbol_manager.symbol_list[2].symbol, symbol_manager.symbol_list[3].symbol,
                                 symbol_manager.symbol_list[4].symbol, symbol_manager.symbol_list[5].symbol,
                                 symbol_manager.symbol_list[6].symbol, symbol_manager.symbol_list[7].symbol,
                                 symbol_manager.symbol_list[8].symbol},
                                {symbol_manager.symbol_list[0].symbol, symbol_manager.symbol_list[1].symbol,
                                 symbol_manager.symbol_list[2].symbol, symbol_manager.symbol_list[3].symbol,
                                 symbol_manager.symbol_list[4].symbol, symbol_manager.symbol_list[5].symbol,
                                 symbol_manager.symbol_list[6].symbol, symbol_manager.symbol_list[7].symbol,
                                 symbol_manager.symbol_list[8].symbol},
                                {symbol_manager.symbol_list[0].symbol, symbol_manager.symbol_list[1].symbol,
                                 symbol_manager.symbol_list[2].symbol, symbol_manager.symbol_list[3].symbol,
                                 symbol_manager.symbol_list[4].symbol, symbol_manager.symbol_list[5].symbol,
                                 symbol_manager.symbol_list[6].symbol, symbol_manager.symbol_list[7].symbol,
                                 symbol_manager.symbol_list[8].symbol}]

        self.face_directions = [0] + Cube.N_FACES * [0]

        self.cube_locations_3D = [
            # z = 0, face = 6
            [
                # x = 0                         x = 1                       x = 2
                [CornerCubeLocation([6, 5, 4]), EdgeCubeLocation([6, 4]), CornerCubeLocation([6, 4, 2])],  # y = 0
                [EdgeCubeLocation([6, 5]), CenterCubeLocation([6]), EdgeCubeLocation([6, 2])],  # y = 1
                [CornerCubeLocation([6, 5, 3]), EdgeCubeLocation([6, 3]), CornerCubeLocation([6, 3, 2])]  # y = 2
            ],
            # z = 1
            [
                [EdgeCubeLocation([5, 4]), CenterCubeLocation([4]), EdgeCubeLocation([4, 2])],
                [CenterCubeLocation([5]), CoreCubeLocation(), CenterCubeLocation([2])],
                [EdgeCubeLocation([5, 3]), CenterCubeLocation([3]), EdgeCubeLocation([3, 2])]
            ],
            # z = 2, face = 1
            [
                [CornerCubeLocation([5, 4, 1]), EdgeCubeLocation([4, 1]), CornerCubeLocation([4, 2, 1])],
                [EdgeCubeLocation([5, 1]), CenterCubeLocation([1]), EdgeCubeLocation([2, 1])],
                [CornerCubeLocation([5, 3, 1]), EdgeCubeLocation([3, 1]), CornerCubeLocation([3, 2, 1])]
            ]
        ]

        self.metadata_text_block = SudokubiMetadata()
        self.metadata_text_block.write(str(id(self)), 8, 0)

        self.sdkb_text_block = TextBlock(4 * (3 * FaceTextBlock.X_MAX - 2) + 3,
                                         3 * (3 * FaceTextBlock.Y_MAX - 2) + 2)
        self.sdkb_face_text_block = [TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2),
                                     TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2),
                                     TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2),
                                     TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2),
                                     TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2),
                                     TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2),
                                     TextBlock(3 * FaceTextBlock.X_MAX - 2, 3 * FaceTextBlock.Y_MAX - 2)]
        assert (self.invariant())

    def __repr__(self):
        return f'Sudokubi, id: {id(self)}'

    def __str__(self):
        Sudokubi.count_str += 1
        self._update_text_block()
        self.sdkb_text_block.write('#' + str(Sudokubi.count_str), 0, 8 * FaceTextBlock.Y_MAX + 2)
        if self.show_metadata_text_block:
            self.metadata_text_block._update()
            return str(self.metadata_text_block) + str(self.sdkb_text_block)
        return str(self.sdkb_text_block)

    def show_metadata(self, show_metadata=True):
        self.show_metadata_text_block = show_metadata

    def show(self):
        if self.show_metadata_text_block:
            print(self.metadata_text_block)
        print(self)
        if Sudokubi.wait_for_key_pressed:
            input("Press <Enter> to continue...")

    def __eq__(self, other) -> bool:
        if not isinstance(other, Sudokubi):
            return False

        self._update_text_block()
        str1 = str(self.sdkb_text_block)
        str1 = str1[:str1.rfind('#')]

        other._update_text_block()
        str2 = str(other.sdkb_text_block)
        str2 = str2[:str2.rfind('#')]

        result = str1 == str2
        return result

    def show_metadata(self, show=True):
        self.show_metadata_text_block = show

    def show(self):
        if self.show_metadata_text_block:
            print(self.metadata_text_block)
        print(self)
        if Sudokubi.wait_for_key_pressed:
            input("Press <Enter> to continue...")

    def add_to_id(self, txt, default=True):
        assert type(txt) == str

        if default:
            self.id += '-' + txt
        else:
            self.id += txt

    def is_correct_placement(self, cube, cube_location):
        if cube_location.cube is not None:
            return False

        if cube.get_type() != cube_location.get_type():
            return False

        cube_faces = cube.get_faces()

        cube_faces.sort()
        cube_location.faces.sort()

        is_correct = False

        if cube_faces == cube_location.faces:
            for f in cube_faces:
                v, d = cube.face_get_v_d(f)
                if v.symbol in self.face_candidates[f]:
                    if d == self.face_directions[f] or self.face_directions[f] == 0:
                        is_correct = True
                    else:
                        is_correct = False
                        break
                else:
                    is_correct = False
                    break

        return is_correct

    def place_cube(self, x, y, z, cube) -> bool:
        assert x in [0, 1, 2]
        assert y in [0, 1, 2]
        assert z in [0, 1, 2]

        cube_location = self.get_cube_location(x, y, z)
        if cube_location.get_type() != cube.get_type():
            assert (self.invariant())
            return False

        if self.is_correct_placement(cube, cube_location):

            cube_location.cube = cube
            faces = cube.get_faces()
            for face in faces:
                v, d = cube.face_get_v_d(face)
                self.set_face_direction(face, d)
                self.remove_candidate_in_face(face, v)
            assert (self.invariant())
            return True
        assert (self.invariant())
        return False

    def get_cube_location(self, x, y, z):
        assert x in [0, 1, 2]
        assert y in [0, 1, 2]
        assert z in [0, 1, 2]
        return self.cube_locations_3D[z][y][x]

    def set_face_direction(self, face: int, direction: int):
        assert 0 < face < 7 and 0 <= direction < 7
        self.face_directions[face] = direction

    def get_face_direction(self, face: int):
        assert 0 < face < 7
        return self.face_directions[face]

    def remove_candidate_in_face(self, face: int, candidate: Symbol):
        assert 0 < face < 7
        self.face_candidates[face].discard(candidate.symbol)

    def is_solved(self) -> bool:
        count_matched = 0
        for x in [0, 1, 2]:
            for y in [0, 1, 2]:
                for z in [0, 1, 2]:
                    if self.get_cube_location(x, y, z).is_matched():
                        count_matched += 1
        return count_matched == 27

    def _update_text_block(self):
        face = 1
        # face 1: row 0
        ftb = self.create_face_text_block(0, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 0)
        ftb = self.create_face_text_block(1, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 0)
        ftb = self.create_face_text_block(2, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 0)

        # face 1: row 1
        ftb = self.create_face_text_block(0, 1, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(1, 1, 2, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(2, 1, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), FaceTextBlock.Y_MAX - 1)

        # face 1: row 2
        ftb = self.create_face_text_block(0, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(1, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(2, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 2 * (FaceTextBlock.Y_MAX - 1))

        face = 2
        # face 2: row 0
        ftb = self.create_face_text_block(2, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 0)
        ftb = self.create_face_text_block(2, 2, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 0)
        ftb = self.create_face_text_block(2, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 0)

        # face 2: row 1
        ftb = self.create_face_text_block(2, 1, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(2, 1, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(2, 1, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), FaceTextBlock.Y_MAX - 1)

        # face 2: row 2
        ftb = self.create_face_text_block(2, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(2, 0, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(2, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 2 * (FaceTextBlock.Y_MAX - 1))

        face = 3
        # face 3: row 0
        ftb = self.create_face_text_block(0, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 0)
        ftb = self.create_face_text_block(1, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 0)
        ftb = self.create_face_text_block(2, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 0)

        # face 3: row 1
        ftb = self.create_face_text_block(0, 2, 1, face)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(1, 2, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(2, 2, 1, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), FaceTextBlock.Y_MAX - 1)

        # face 3: row 2
        ftb = self.create_face_text_block(0, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(1, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(2, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 2 * (FaceTextBlock.Y_MAX - 1))

        face = 4
        # face 4: row 0
        ftb = self.create_face_text_block(0, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 0)
        ftb = self.create_face_text_block(1, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 0)
        ftb = self.create_face_text_block(2, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 0)

        # face 4: row 1
        ftb = self.create_face_text_block(0, 0, 1, face)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(1, 0, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(2, 0, 1, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), FaceTextBlock.Y_MAX - 1)

        # face 4: row 2
        ftb = self.create_face_text_block(0, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(1, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(2, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 2 * (FaceTextBlock.Y_MAX - 1))

        face = 5
        # face 5: row 0
        ftb = self.create_face_text_block(0, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 0)
        ftb = self.create_face_text_block(0, 2, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 0)
        ftb = self.create_face_text_block(0, 2, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 0)

        # face 5: row 1
        ftb = self.create_face_text_block(0, 1, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(0, 1, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(0, 1, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), FaceTextBlock.Y_MAX - 1)

        # face 5: row 2
        ftb = self.create_face_text_block(0, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(0, 0, 1, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(0, 0, 2, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 2 * (FaceTextBlock.Y_MAX - 1))

        face = 6
        # face 6: row 0
        ftb = self.create_face_text_block(2, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 0)
        ftb = self.create_face_text_block(1, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 0)
        ftb = self.create_face_text_block(0, 2, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 0)

        # face 6: row 1
        ftb = self.create_face_text_block(2, 1, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(1, 1, 0, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, FaceTextBlock.Y_MAX - 1)
        ftb = self.create_face_text_block(0, 1, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), FaceTextBlock.Y_MAX - 1)

        # face 6: row 2
        ftb = self.create_face_text_block(2, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 0, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(1, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, FaceTextBlock.X_MAX - 1, 2 * (FaceTextBlock.Y_MAX - 1))
        ftb = self.create_face_text_block(0, 0, 0, face)
        self.sdkb_face_text_block[face].add(ftb, 2 * (FaceTextBlock.X_MAX - 1), 2 * (FaceTextBlock.Y_MAX - 1))

        # face 1
        self.sdkb_face_text_block[1].write('1', 0, 0)
        self.sdkb_text_block.add(self.sdkb_face_text_block[1], 3 * FaceTextBlock.X_MAX - 2 + 1,
                                 3 * FaceTextBlock.Y_MAX - 2 + 1)
        # face 2
        self.sdkb_face_text_block[2].write('2', 0, 0)
        self.sdkb_text_block.add(self.sdkb_face_text_block[2], 2 * (3 * FaceTextBlock.X_MAX - 2) + 2,
                                 3 * FaceTextBlock.Y_MAX - 2 + 1)
        # face 3
        self.sdkb_face_text_block[3].write('3', 0, 0)
        self.sdkb_text_block.add(self.sdkb_face_text_block[3], 3 * FaceTextBlock.X_MAX - 1, 1)
        # face 4
        self.sdkb_face_text_block[4].write('4', 0, 0)
        self.sdkb_text_block.add(self.sdkb_face_text_block[4], 3 * FaceTextBlock.X_MAX - 1,
                                 2 * (3 * FaceTextBlock.Y_MAX - 2) + 1)
        # face 5
        self.sdkb_face_text_block[5].write('5', 0, 0)
        self.sdkb_text_block.add(self.sdkb_face_text_block[5], 0, 3 * FaceTextBlock.Y_MAX - 2 + 1)
        # face 6
        self.sdkb_face_text_block[6].write('6', 0, 0)
        self.sdkb_text_block.add(self.sdkb_face_text_block[6], 3 * (3 * FaceTextBlock.X_MAX - 2) + 3,
                                 3 * FaceTextBlock.Y_MAX - 2 + 1)

        # Metadata
        self.metadata_text_block.write(str(id(self)), 8, 0)

    def _create_candidates_text_block(self, candidates: list):
        ctb = TextBlock(3, 3)
        x = 0
        y = 0
        for c in self.ALL_CANDIDATES:
            if c in candidates:
                ctb.write(str(c), x, y)
            else:
                ctb.write('.', x, y)
            x = x + 1
            if x == 3:
                x = 0
                y += 1
        return ctb

    @staticmethod
    def _create_value_text_block(value: int):
        vtb = TextBlock(3, 3)
        vtb.write(str(value), 1, 1)
        return vtb

    def create_face_text_block(self, x, y, z, face):
        ftb = FaceTextBlock()
        placeholder = self.get_cube_location(x, y, z)

        if placeholder.is_matched():
            v, d = placeholder.cube.face_get_v_d(face)
            # ftb.write(str(v), int(FaceTextBlock.X_MAX * 1.5), FaceTextBlock.Y_MAX + 3)
            # ftb.write(str(d), FaceTextBlock.X_MAX + 1, FaceTextBlock.Y_MAX + 1)
            d = self.face_directions[face]
            if d != 0:
                ftb.write(str(d), 1, 1)
            else:
                ftb.write(str('-'), 1, 1)
            ftb.add(self._create_value_text_block(v), 2, 2)
        else:
            face_indices = placeholder.faces
            candidates = []
            for index in face_indices:
                candidates.append(self.face_candidates[index])
            ctb = self._create_candidates_text_block(candidates)
            ftb.add(self._create_candidates_text_block(self.face_candidates[face]), 2, 2)
            d = self.face_directions[face]
            if d != 0:
                ftb.write(str(d), 1, 1)
            else:
                ftb.write(str('-'), 1, 1)
            ftb.add(ctb, 2, 2)

        return ftb

    def invariant(self):
        ids_placeholders = set()
        ids_cubes = set()
        for x in [0, 1, 2]:
            for y in [0, 1, 2]:
                for z in [0, 1, 2]:
                    if self.get_cube_location(x, y, z).is_matched():
                        ids_cubes.add(id(self.get_cube_location(x, y, z).cube))
                    else:
                        ids_placeholders.add(id(self.get_cube_location(x, y, z)))
        return len(ids_placeholders) + len(ids_cubes) == 27
