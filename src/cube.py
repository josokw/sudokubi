from abc import abstractmethod
from enum import IntEnum
from typing import *

from cube_text_block import *
from symbol_manager import *


class Cube(ABC):
    """
    Abstract Base Class: represents the interface of a cube (6 faces).
    Faces can show a value and a direction or are blanc: face value direction.
    Face ids like dice face numbering [1..6], "face 0" not used.
    """

    N_FACES = 6
    ALL_FACES = [f for f in range(1, N_FACES + 1)]
    MAX_CANDIDATE = 9

    class Type(IntEnum):
        CORE_CUBE_0 = 0
        CENTER_CUBE_1 = 1
        EDGE_CUBE_2 = 2
        CORNER_CUBE_3 = 3
        TEST_CUBE_6 = 6

    def __init__(self, symbol_manager=SymbolManager,
                 face_value_direction_s=Optional[List[List[Union[int, Symbol]]]]) -> None:
        assert isinstance(symbol_manager, SymbolManager)
        self.symbol_set_data = symbol_manager

        self.fvds = [[0, 0]] + Cube.N_FACES * [[0, 0]]
        if face_value_direction_s is not None:
            for f, v, d in face_value_direction_s:
                assert type(f) == type(d) == int
                assert type(v) == Symbol
                assert 0 < f <= Cube.N_FACES
                assert 0 < d <= Cube.N_FACES
                self.fvds[f] = [v, d]

        assert len(face_value_direction_s) == self.get_type()
        self.text_block = CubeTextBlock()
        self._invariant()

    @abstractmethod
    def __repr__(self) -> str:
        pass

    def __str__(self) -> str:
        self._update_txt()
        return str(self.text_block)

    def __eq__(self, other) -> bool:
        if isinstance(other, Cube):
            if not self.get_type() == other.get_type():
                return False
            for idx in range(1, Cube.N_FACES + 1):
                if not self.fvds[idx][0] == other.fvds[idx][0]:
                    return False
                if not self.fvds[idx][1] == other.fvds[idx][1]:
                    return False
            return True
        return False

    def get_text_block(self) -> CubeTextBlock:
        self._update_txt()
        return self.text_block

    @abstractmethod
    def get_type(self) -> int:
        pass

    def face_get_v_d(self, face) -> List[Union[int, Symbol]]:
        assert 0 < face <= Cube.N_FACES
        return self.fvds[face]

    def rotations_values_faces_indices(self) -> List[List[Union[Symbol, List[int]]]]:
        rotations_per_value = list()
        for some_symbol in self.get_values():
            rotations_for_idx = list()
            for rot_idx in range(8):
                if not self.symbol_set_data.values_symmetries[
                           self.symbol_set_data.get_symbol_index(some_symbol)][rot_idx] \
                       == self.symbol_set_data.FORBIDDEN_SYMBOL:
                    rotations_for_idx.append(rot_idx)
            faces_per_value = self.value_get_f_s(some_symbol)
            rotations_per_value.append([some_symbol, faces_per_value, rotations_for_idx])

        return rotations_per_value

    def separate_rotations(self):
        rotations_overview = self.rotations_values_faces_indices()
        split_rotations_list = list()
        for value_idx in rotations_overview:
            for face_idx in range(len(value_idx[1])):
                split_rotations_list.append([value_idx[0],
                                             value_idx[1][face_idx],
                                             value_idx[2]])
        return split_rotations_list

    def all_rotation_combinations(self):
        split_rotations_list = self.separate_rotations()
        all_rot_comb = list()

        if self.get_type() == 0:
            pass
        elif self.get_type() == 1:
            for rot in split_rotations_list[0][2]:
                all_rot_comb.append([[split_rotations_list[0][0], split_rotations_list[0][1], rot]])
        elif self.get_type() == 2:
            for idx_0 in range(len(split_rotations_list[0][2])):
                for idx_1 in range(len(split_rotations_list[1][2])):
                    all_rot_comb.append([[split_rotations_list[0][0],
                                          split_rotations_list[0][1],
                                          split_rotations_list[0][2][idx_0]],
                                         [split_rotations_list[1][0],
                                          split_rotations_list[1][1],
                                          split_rotations_list[1][2][idx_1]]])
        elif self.get_type() == 3:
            for idx_0 in range(len(split_rotations_list[0][2])):
                for idx_1 in range(len(split_rotations_list[1][2])):
                    for idx_2 in range(len(split_rotations_list[2][2])):
                        all_rot_comb.append([[split_rotations_list[0][0],
                                              split_rotations_list[0][1],
                                              split_rotations_list[0][2][idx_0]],
                                             [split_rotations_list[1][0],
                                              split_rotations_list[1][1],
                                              split_rotations_list[1][2][idx_1]],
                                             [split_rotations_list[2][0],
                                              split_rotations_list[2][1],
                                              split_rotations_list[2][2][idx_2]]
                                             ])
        else:
            pass

        return all_rot_comb

    def value_get_f_s(self, value: Symbol) -> List[int]:
        faces_list: List[int] = list()
        for f in range(1, Cube.N_FACES + 1):
            if type(self.fvds[f][0]) == Symbol:
                if self.fvds[f][0] == value:
                    faces_list.append(f)
        return faces_list

    def get_values(self) -> List[Symbol]:
        value_list: List[Symbol] = list()
        for v, d in self.fvds:
            if isinstance(v, Symbol) and v not in value_list:
                value_list.append(v)
        return value_list

    def get_faces(self) -> List[int]:
        face_list = list()
        for f in range(1, Cube.N_FACES + 1):
            if type(self.fvds[f][0]) == Symbol:
                face_list.append(f)
        return face_list

    def get_directions(self) -> List[int]:
        direction_list = list()
        for v, d in self.fvds:
            if not v == 0 and d not in direction_list:
                direction_list.append(d)
        return direction_list

    def rotate_symbol(self, value: Symbol, face: int, rotation_index: int) -> None:
        assert self.fvds[face][0] == value
        assert 0 < face < 7
        assert 0 <= rotation_index <= 7

        possible_other_values: List[Symbol] = self.symbol_set_data.values_symmetries[
            self.symbol_set_data.get_symbol_index(value)]
        assert not possible_other_values[rotation_index] == self.symbol_set_data.FORBIDDEN_SYMBOL

        if rotation_index == 0:
            pass
        elif rotation_index == 1:
            direction_list = list()
            if face == 1:
                direction_list = [3, 2, 4, 5, 3]
            elif face == 2:
                direction_list = [3, 6, 4, 1, 3]
            elif face == 3:
                direction_list = [6, 2, 1, 5, 6]
            elif face == 4:
                direction_list = [6, 5, 1, 2, 6]
            elif face == 5:
                direction_list = [3, 1, 4, 6, 3]
            elif face == 6:
                direction_list = [3, 5, 4, 2, 3]
            self.fvds[face][1] = direction_list[direction_list.index(self.fvds[face][1]) + 1]
        elif rotation_index == 2:
            self.fvds[face][1] = 7 - self.fvds[face][1]
        elif rotation_index == 3:
            direction_list = list()
            if face == 6:
                direction_list = [3, 2, 4, 5, 3]
            elif face == 5:
                direction_list = [3, 6, 4, 1, 3]
            elif face == 4:
                direction_list = [6, 2, 1, 5, 6]
            elif face == 3:
                direction_list = [6, 5, 1, 2, 6]
            elif face == 2:
                direction_list = [3, 1, 4, 6, 3]
            elif face == 1:
                direction_list = [3, 5, 4, 2, 3]
            self.fvds[face][1] = direction_list[direction_list.index(self.fvds[face][1]) + 1]
        elif rotation_index > 3:
            pass  # todo: implement mirror symmetry
        self.fvds[face][0] = possible_other_values[rotation_index]
        return

    def _invariant(self) -> None:
        assert True

    def _update_txt(self) -> None:
        self.text_block.clear()
        for f in Cube.ALL_FACES:
            v, d = self.fvds[f]
            if type(v) == Symbol:
                if f == 1:
                    self.text_block.write(str(v), int(FaceTextBlock.X_MAX * 1.5), FaceTextBlock.Y_MAX + 3)
                    self.text_block.write(str(d), FaceTextBlock.X_MAX + 1, FaceTextBlock.Y_MAX + 1)
                elif f == 2:
                    self.text_block.write(str(v), int(FaceTextBlock.X_MAX * 2.5), FaceTextBlock.Y_MAX + 3)
                    self.text_block.write(str(d), int(FaceTextBlock.X_MAX * 2) + 1, FaceTextBlock.Y_MAX + 1)
                elif f == 3:
                    self.text_block.write(str(v), int(FaceTextBlock.X_MAX * 1.5), 3)
                    self.text_block.write(str(d), int(FaceTextBlock.X_MAX + 1), 1)
                elif f == 4:
                    self.text_block.write(str(v), int(FaceTextBlock.X_MAX * 1.5), (FaceTextBlock.Y_MAX * 2) + 3)
                    self.text_block.write(str(d), FaceTextBlock.X_MAX + 1, (FaceTextBlock.Y_MAX * 2) + 1)
                elif f == 5:
                    self.text_block.write(str(v), int(FaceTextBlock.X_MAX * 0.5), FaceTextBlock.Y_MAX + 3)
                    self.text_block.write(str(d), 1, FaceTextBlock.Y_MAX + 1)
                elif f == 6:
                    self.text_block.write(str(v), int(FaceTextBlock.X_MAX * 3.5), FaceTextBlock.Y_MAX + 3)
                    self.text_block.write(str(d), int(FaceTextBlock.X_MAX * 3) + 1, FaceTextBlock.Y_MAX + 1)

    def yaw(self) -> None:
        """ Rotate axis 1->6 """
        moves = {0: 0, 1: 1, 2: 4, 3: 2, 4: 5, 5: 3, 6: 6}
        new_fvds = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
        for f in self.get_faces():
            new_fvds[moves[f]][0] = self.fvds[f][0]
            new_fvds[moves[f]][1] = moves[self.fvds[f][1]]
        self.fvds = new_fvds

    def pitch(self) -> None:
        """ Rotate axis 3->4 """
        moves = {0: 0, 1: 5, 2: 1, 3: 3, 4: 4, 5: 6, 6: 2}
        new_fvds = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
        for f in self.get_faces():
            new_fvds[moves[f]][0] = self.fvds[f][0]
            new_fvds[moves[f]][1] = moves[self.fvds[f][1]]
        self.fvds = new_fvds

    def roll(self) -> None:
        """ Rotate axis 2->5 """
        moves = {0: 0, 1: 4, 2: 2, 3: 1, 4: 6, 5: 5, 6: 3}
        new_fvds = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
        for f in self.get_faces():
            new_fvds[moves[f]][0] = self.fvds[f][0]
            new_fvds[moves[f]][1] = moves[self.fvds[f][1]]
        self.fvds = new_fvds


class CoreCube(Cube):
    """
        Represents a cube (6 faces).
        0 faces used. Inside core Sudokubi.
    """

    def __init__(self, symbol_manager, face_value_direction_s=None):
        assert face_value_direction_s is None
        super().__init__(symbol_manager, face_value_direction_s)

    def __str__(self):
        return super().__str__()

    def __repr__(self):
        return f'CoreCube: None'

    def get_type(self):
        return Cube.Type.CORE_CUBE_0


class CenterCube(Cube):
    """
        Represents a cube (6 faces).
        Faces can show a value or are blanc: face value direction.
        Face ids like dice face numbering [1..6], "face 0" not used.
        Only 1 face used.
    """

    def __init__(self, symbol_manager, face_value_direction_s=None):
        if face_value_direction_s is not None:
            assert len(face_value_direction_s) == 1
        super().__init__(symbol_manager, face_value_direction_s)

    def __str__(self):
        return super().__str__()

    def get_type(self):
        return Cube.Type.CENTER_CUBE_1

    def __repr__(self):
        return f'CenterCube: {self.get_faces()}'


class EdgeCube(Cube):
    """
        Represents a cube (6 faces).
        Faces can show a value or are blanc: face value direction.
        Face ids like dice face numbering [1..6], "face 0" not used.
        Only 2 faces used.
    """

    def __init__(self, symbol_manager, face_value_direction_s=None):
        if face_value_direction_s is not None:
            assert len(face_value_direction_s) == 2
        super().__init__(symbol_manager, face_value_direction_s)

    def __str__(self):
        return super().__str__()

    def get_type(self):
        return Cube.Type.EDGE_CUBE_2

    def __repr__(self):
        return f'EdgeCube: {self.get_faces()}'


class CornerCube(Cube):
    """
        Represents a cube (6 faces).
        Faces can show a value or are blanc: face value direction.
        Face ids like dice face numbering [1..6], "face 0" not used.
        Only 3 faces used.
    """

    def __init__(self, symbol_manager, face_value_direction_s=None):
        if face_value_direction_s is not None:
            assert len(face_value_direction_s) == 3
        super().__init__(symbol_manager, face_value_direction_s)

    def __repr__(self):
        return f'CornerCube: {self.get_faces()}'

    def __str__(self):
        return super().__str__()

    def get_type(self):
        return Cube.Type.CORNER_CUBE_3


class TestCube(CoreCube):
    """
        Represents a cube (6 faces).
        All faces show a value == face id and a direction.
    """

    def __init__(self, symbol_manager):
        super().__init__(symbol_manager)
        for f, v, d in [[1, 1, 5], [2, 2, 1], [3, 3, 2], [4, 4, 3], [5, 5, 6], [6, 6, 4]]:
            self.fvds[f] = [v, d]

    def get_type(self):
        return Cube.Type.TEST_CUBE_6

    def __repr__(self):
        return f'TestCube: {self.get_faces()}'
