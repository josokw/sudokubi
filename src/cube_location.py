from abc import ABC, abstractmethod

from cube import *


class CubeLocation(ABC):
    """
    Abstract Base Class (ABC) for containing a list of faces, a matched status and a Cube derived object if matched.
    Face 0 is not used.
    """

    class Type(IntEnum):
        CORE_LOCATION_0 = 0
        CENTER_LOCATION_1 = 1
        EDGE_LOCATION_2 = 2
        CORNER_LOCATION_3 = 3

    def __init__(self):
        self.cube = None
        self.faces = []

    def __repr__(self):
        return f'Placeholder: self.cube = {self.cube}, self.faces = {self.faces}'

    def __str__(self):
        if self.cube is not None:
            return str(self.cube)
        return str(self.faces)

    def is_matched(self) -> bool:
        return self.cube is not None

    @abstractmethod
    def get_type(self):
        pass


class CoreCubeLocation(CubeLocation):
    """
    Contains a CorePlaceholder object and no faces.
    """

    def __init__(self, faces=None):
        assert faces is None
        super().__init__()

    def get_type(self):
        return CubeLocation.Type.CORE_LOCATION_0


class CenterCubeLocation(CubeLocation):
    """
    Contains a CenterPlaceholder object and a list with a single face.
    """

    def __init__(self, faces):
        assert len(faces) == 1
        assert 0 < faces[0] <= Cube.N_FACES

        super().__init__()
        self.faces = faces

    def get_type(self):
        return CubeLocation.Type.CENTER_LOCATION_1


class EdgeCubeLocation(CubeLocation):
    """
    Contains a EdgePlaceholder object and 2 faces
    """

    def __init__(self, faces):
        assert len(faces) == 2
        assert 0 < faces[0] <= Cube.N_FACES
        assert 0 < faces[1] <= Cube.N_FACES

        super().__init__()
        self.faces = faces

    def set_cube(self, cube):
        assert cube.get_type() == self.get_type()
        self.cube = cube

    def get_type(self):
        return CubeLocation.Type.EDGE_LOCATION_2


class CornerCubeLocation(CubeLocation):
    """
    Contains a CornerPlaceholder object and 3 faces.
    """

    def __init__(self, faces):
        assert len(faces) == 3
        assert 0 < faces[0] <= Cube.N_FACES
        assert 0 < faces[1] <= Cube.N_FACES
        assert 0 < faces[2] <= Cube.N_FACES

        super().__init__()
        self.faces = faces
        self.faces.sort()

    def set_cube(self, cube):
        assert cube.get_type() == self.get_type()
        self.cube = cube

    def get_type(self):
        return CubeLocation.Type.CORNER_LOCATION_3
