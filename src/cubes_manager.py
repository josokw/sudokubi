from sudokubi_generator import *


class CubesManager(ABC):
    """
    Contains all cubes and all their rotations (hard coded).
    """

    N_CUBES_1 = 6
    N_CUBES_2 = 12
    N_CUBES_3 = 8

    def __init__(self, symbol_manager):
        assert isinstance(symbol_manager, SymbolManager)
        self.symbol_manager = symbol_manager
        self.cubes1 = list()
        self.cubes2 = list()
        self.cubes3 = list()

    def __repr__(self):
        return f'CubesManager, ' \
               f'#cubes1 = {len(self.cubes1)}, ' \
               f'#cubes2 = {len(self.cubes2)}, ' \
               f'#cubes3 = {len(self.cubes3)}, '

    @staticmethod
    def calc_all_rotations(initial_cube) -> list:
        all_rotations = list()
        instruction_list = initial_cube.all_rotation_combinations()

        for cube_idx in range(len(instruction_list)):
            copy_initial_cube = copy.deepcopy(initial_cube)
            for face_idx in range(initial_cube.get_type()):
                copy_initial_cube.rotate_symbol(instruction_list[cube_idx][face_idx][0],
                                                instruction_list[cube_idx][face_idx][1],
                                                instruction_list[cube_idx][face_idx][2])
            all_rotations.extend(CubesManager._calc_rotations(copy_initial_cube))

        return all_rotations

    def _invariant(self):
        assert len(self.cubes1) == CubesManager.N_CUBES_1
        assert len(self.cubes2) == CubesManager.N_CUBES_2
        assert len(self.cubes3) == CubesManager.N_CUBES_3

    @staticmethod
    def _calc_rotations(initial_cube) -> list:
        all_rotations = list()
        all_rotations.append(copy.deepcopy(initial_cube))
        all_rotations.extend(CubesManager._pitch3x_roll3x(initial_cube))
        for cb in all_rotations:
            cb_start_rot = copy.deepcopy(cb)
            cb_start_rot.yaw()
            if cb_start_rot not in all_rotations:
                all_rotations.append(copy.deepcopy(cb_start_rot))
        return all_rotations

    @staticmethod
    def _pitch3x_roll3x(cb) -> list:
        all_rotations = list()
        cb_start_pitch = copy.deepcopy(cb)
        cb_start_roll = copy.deepcopy(cb)
        for p in range(1, 4):
            cb_start_pitch.pitch()
            # self._count += 1
            if cb_start_pitch not in all_rotations:
                all_rotations.append(copy.deepcopy(cb_start_pitch))
        for r in range(1, 4):
            cb_start_roll.roll()
            # self._count += 1
            if cb_start_roll not in all_rotations:
                all_rotations.append(copy.deepcopy(cb_start_roll))
        return all_rotations


class OriginalSet(CubesManager):
    # NOTE: This set of Cubes can be called with any set of Cubes (other Symbols), but
    # its current definition relies on the symbols with index 5 and index 8 (numbers 6 and 9
    # in the original set) are each other's flip-symbol (rotation 180 degrees). If this condition
    # is not satisfied, no solutions might be given.

    def __init__(self, symbol_manager):
        super().__init__(symbol_manager)

        self.cubes1 = [
            [CenterCube(symbol_manager, [[1, self.symbol_manager.symbol_list[2], 5]])],
            [CenterCube(symbol_manager, [[1, self.symbol_manager.symbol_list[3], 5]])],
            [CenterCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5]])],
            [CenterCube(symbol_manager, [[1, self.symbol_manager.symbol_list[6], 5]])],
            [CenterCube(symbol_manager, [[1, self.symbol_manager.symbol_list[7], 5]])],
            [CenterCube(symbol_manager, [[1, self.symbol_manager.symbol_list[7], 5]])]
        ]
        for idx, _ in enumerate(self.cubes1):
            self.cubes1[idx] = self.calc_all_rotations(self.cubes1[idx][0])

        self.cubes2 = [
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[2], 5],
                                       [2, self.symbol_manager.symbol_list[1], 3]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[4], 5],
                                       [4, self.symbol_manager.symbol_list[3], 5]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                       [2, self.symbol_manager.symbol_list[4], 4]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                       [4, self.symbol_manager.symbol_list[3], 1]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                       [2, self.symbol_manager.symbol_list[0], 6]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                       [3, self.symbol_manager.symbol_list[4], 5]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                       [2, self.symbol_manager.symbol_list[2], 6]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[6], 5],
                                       [3, self.symbol_manager.symbol_list[3], 2]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[6], 5],
                                       [3, self.symbol_manager.symbol_list[4], 5]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[6], 5],
                                       [5, self.symbol_manager.symbol_list[5], 6]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[6], 5],
                                       [2, self.symbol_manager.symbol_list[5], 6]])],
            [EdgeCube(symbol_manager, [[1, self.symbol_manager.symbol_list[7], 5],
                                       [3, self.symbol_manager.symbol_list[3], 1]])]]
        for idx, _ in enumerate(self.cubes2):
            self.cubes2[idx] = self.calc_all_rotations(self.cubes2[idx][0])

        self.cubes3 = [
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                         [2, self.symbol_manager.symbol_list[0], 1],
                                         [3, self.symbol_manager.symbol_list[1], 2]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[3], 5],
                                         [4, self.symbol_manager.symbol_list[1], 1],
                                         [5, self.symbol_manager.symbol_list[1], 6]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                         [4, self.symbol_manager.symbol_list[0], 2],
                                         [5, self.symbol_manager.symbol_list[2], 6]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[7], 5],
                                         [2, self.symbol_manager.symbol_list[4], 1],
                                         [4, self.symbol_manager.symbol_list[5], 2]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[6], 5],
                                         [2, self.symbol_manager.symbol_list[4], 1],
                                         [3, self.symbol_manager.symbol_list[2], 1]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[5], 5],
                                         [4, self.symbol_manager.symbol_list[1], 5],
                                         [5, self.symbol_manager.symbol_list[1], 3]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[2], 5],
                                         [2, self.symbol_manager.symbol_list[0], 1],
                                         [3, self.symbol_manager.symbol_list[0], 5]])],
            [CornerCube(symbol_manager, [[1, self.symbol_manager.symbol_list[7], 5],
                                         [2, self.symbol_manager.symbol_list[0], 1],
                                         [4, self.symbol_manager.symbol_list[7], 1]])]]
        for idx, _ in enumerate(self.cubes3):
            self.cubes3[idx] = self.calc_all_rotations(self.cubes3[idx][0])

        self._invariant()


class RandomSet(CubesManager):
    def __init__(self, symbol_manager):
        super().__init__(symbol_manager)

        random_sudokubi = RandomSudokubi(symbol_manager.symbol_list)
        for prcocu in random_sudokubi.proto_corner_cubes:
            self.cubes3.append([CornerCube(symbol_manager, prcocu)])
        for predcu in random_sudokubi.proto_edge_cubes:
            self.cubes2.append([EdgeCube(symbol_manager, predcu)])
        for prcecu in random_sudokubi.proto_center_cubes:
            self.cubes1.append([CenterCube(symbol_manager, prcecu)])

        for idx, _ in enumerate(self.cubes1):
            self.cubes1[idx] = self.calc_all_rotations(self.cubes1[idx][0])
        for idx, _ in enumerate(self.cubes2):
            self.cubes2[idx] = self.calc_all_rotations(self.cubes2[idx][0])
        for idx, _ in enumerate(self.cubes3):
            self.cubes3[idx] = self.calc_all_rotations(self.cubes3[idx][0])
