from abc import ABC
from typing import List

from symbol import *


# todo: implement mirror symmetry indices 4-7

class SymbolManager(ABC):
    """
    Contains all symbols and their relations (identities through rotational/mirror symmetries).
    Allows changing and removing relations between values.

    Meaning values_symmetries per index (positive rotation = CW):
    #0: original value
    #1: rotation 90 degrees
    #2: rotation 180 degrees
    #3: rotation 270 degrees
    #4: mirror image
    #5: mirror, then rotate 90
    #6: mirror, then rotate 180
    #7: mirror, then rotate 270
    """

    N_SYMBOLS: int = 9

    def __init__(self, list_of_symbols: List[Symbol], list_of_relations: List[List[Symbol]]):
        self.FORBIDDEN_SYMBOL = Symbol('-')
        self.symbol_list = list_of_symbols
        self.values_symmetries = list_of_relations
        self.validate_list_of_symbols()
        self.symbol_symbol_list = list()

        if self.symbol_list is not None:
            for idx in range(len(self.symbol_list)):
                self.symbol_symbol_list.append(self.symbol_list[idx].symbol)

        self.validate_list_of_relations()

    def __getitem__(self, index):
        return self.values_symmetries[index]

    def validate_list_of_symbols(self) -> None:
        assert type(self.symbol_list) == list
        for idx in range(len(self.symbol_list)):
            assert isinstance(self.symbol_list[idx], Symbol)
            assert not self.symbol_list[idx] == self.FORBIDDEN_SYMBOL
        assert len(self.symbol_list) == SymbolManager.N_SYMBOLS
        assert self.all_unique_symbols()
        return

    def all_unique_symbols(self) -> bool:
        for idx in range(len(self.symbol_list)):
            for idx2 in range(idx + 1, len(self.symbol_list)):
                if self.symbol_list[idx] == self.symbol_list[idx2]:
                    return False
        return True

    def validate_list_of_relations(self) -> None:
        assert type(self.values_symmetries) == list
        assert len(self.values_symmetries) == SymbolManager.N_SYMBOLS

        # Validate symbology
        for symbol_idx in range(SymbolManager.N_SYMBOLS):
            assert self.values_symmetries[symbol_idx][0].symbol == self.symbol_list[symbol_idx].symbol
            for symmetry_idx in range(1, 8):
                assert (self.values_symmetries[symbol_idx][symmetry_idx] == self.FORBIDDEN_SYMBOL
                        or self.values_symmetries[symbol_idx][symmetry_idx].symbol in self.symbol_symbol_list)

        # Validate relations reciprocal and physically possible
        for symbol_idx in range(SymbolManager.N_SYMBOLS):
            if not self.values_symmetries[symbol_idx][1] == self.FORBIDDEN_SYMBOL:
                assert self.values_symmetries[self.get_symbol_index(
                    self.values_symmetries[symbol_idx][1])][3] == \
                       self.values_symmetries[symbol_idx][0]
            if not self.values_symmetries[symbol_idx][2] == self.FORBIDDEN_SYMBOL:
                assert self.values_symmetries[self.get_symbol_index(
                    self.values_symmetries[symbol_idx][2])][2] \
                       == self.values_symmetries[symbol_idx][0]
            if not self.values_symmetries[symbol_idx][3] == self.FORBIDDEN_SYMBOL:
                assert self.values_symmetries[self.get_symbol_index(
                    self.values_symmetries[symbol_idx][3])][1] == self.values_symmetries[symbol_idx][0]
        return

    def get_symbol_index(self, search_symbol: Symbol) -> int:
        for idx in range(len(self.values_symmetries)):
            if self.values_symmetries[idx][0] == search_symbol:
                return idx
        raise ValueError("Index of symbol not found")


class StandardSetManager(SymbolManager):
    def __init__(self):
        # construct symbols + symbol manager according to original puzzle
        symbol_one = Symbol('1')
        symbol_two = Symbol('2')
        symbol_three = Symbol('3')
        symbol_four = Symbol('4')
        symbol_five = Symbol('5')
        symbol_six = Symbol('6')
        symbol_seven = Symbol('7')
        symbol_eight = Symbol('8')
        symbol_nine = Symbol('9')

        symbol_list = [symbol_one, symbol_two, symbol_three, symbol_four, symbol_five, symbol_six, symbol_seven,
                       symbol_eight, symbol_nine]

        symbol_relations = list()
        for symbol_idx in range(9):
            new_list = list()
            new_list.append(symbol_list[symbol_idx])
            for sym_idx in range(7):
                new_list.append(Symbol('-'))
            symbol_relations.append(new_list)

        # Relation: 6 is 9 flipped
        symbol_relations[5][2] = symbol_list[8]
        symbol_relations[8][2] = symbol_list[5]

        super().__init__(symbol_list, symbol_relations)
