class Symbol:
    """
    Represents the symbol on a face of a Cube. A Symbol has a certain direction,
    every Symbol has a number of rotational symmetries as well: symmetry under
    rotation after 90 (or 270) deg and/or rotation after 180 deg. Future feature:
    symmetries after mirroring.
    """

    def __init__(self, symbol: str, representation: str = None) -> None:
        self.symbol = symbol  # symbol used as character in code

        if representation is None:
            self.representation = symbol
        else:
            self.representation = representation  # representation defines how to present in output

    def __repr__(self) -> str:
        return str(self.symbol)

    def __str__(self) -> str:
        return str(self.representation)

    def __eq__(self, other) -> bool:
        if type(self) == type(other):
            return self.symbol == other.symbol
        else:
            return False
