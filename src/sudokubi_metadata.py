from cube_text_block import *


class SudokubiMetadata(TextBlock):
    """
    Contains Sudokubi metadata info (specialised TextBlock)
    - id
    - Center cubes indices
    - Edge cubes indices
    - Corner cubes indices
    """

    X_MAX = 200
    Y_MAX = 4

    def __init__(self):
        super().__init__(SudokubiMetadata.X_MAX, SudokubiMetadata.Y_MAX)
        self.write('id:     ', 0, 0)
        self.write('Center: ', 0, 1)
        self.write('Edge:   ', 0, 2)
        self.write('Corner: ', 0, 3)
        self.center_info = ''
        self.edge_info = ''
        self.corner_info = ''

    def __repr__(self):
        return f'SudokubiMetadata: X_MAX={SudokubiMetadata.X_MAX}, Y_MAX={SudokubiMetadata.Y_MAX}'

    def __str__(self) -> str:
        self._update()
        return super().__str__()

    def clear(self):
        self.__init__()

    def add_to_center_info(self, info: str):
        if self.center_info != '':
            self.center_info += '-' + info
        else:
            self.center_info += info

    def add_to_edge_info(self, info: str):
        if self.edge_info != '':
            self.edge_info += '-' + info
        else:
            self.edge_info += info

    def add_to_corner_info(self, info: str):
        if self.corner_info != '':
            self.corner_info += '-' + info
        else:
            self.corner_info += info

    def _update(self):
        self.write(self.center_info, 8, 1)
        self.write(self.edge_info, 8, 2)
        self.write(self.corner_info, 8, 3)
