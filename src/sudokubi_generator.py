import random

from cube import *


class SudokubiGenerator(ABC):

    def __init__(self, symbol_list: List[Union[None, 'Symbol']]) -> None:
        self.face_1 = copy.deepcopy(symbol_list)
        self.face_2 = copy.deepcopy(symbol_list)
        self.face_3 = copy.deepcopy(symbol_list)
        self.face_4 = copy.deepcopy(symbol_list)
        self.face_5 = copy.deepcopy(symbol_list)
        self.face_6 = copy.deepcopy(symbol_list)


class RandomSudokubi(SudokubiGenerator):

    def __init__(self, symbol_list: List[Union[None, 'Symbol']]) -> None:
        super().__init__(symbol_list)

        random.shuffle(self.face_1)
        random.shuffle(self.face_2)
        random.shuffle(self.face_3)
        random.shuffle(self.face_4)
        random.shuffle(self.face_5)
        random.shuffle(self.face_6)

        self.direction_face_1 = random.choice([2, 3, 4, 5])
        self.direction_face_2 = random.choice([1, 3, 4, 6])
        self.direction_face_3 = random.choice([1, 2, 5, 6])
        self.direction_face_4 = random.choice([1, 2, 5, 6])
        self.direction_face_5 = random.choice([1, 3, 4, 6])
        self.direction_face_6 = random.choice([2, 3, 4, 5])

        self.proto_corner_cubes = list()
        self.proto_corner_cubes.append([[1, self.face_1[0], self.direction_face_1],
                                        [3, self.face_3[6], self.direction_face_3],
                                        [5, self.face_5[2], self.direction_face_5]])
        self.proto_corner_cubes.append([[1, self.face_1[2], self.direction_face_1],
                                        [2, self.face_2[0], self.direction_face_2],
                                        [3, self.face_3[8], self.direction_face_3]])
        self.proto_corner_cubes.append([[1, self.face_1[6], self.direction_face_1],
                                        [4, self.face_4[0], self.direction_face_4],
                                        [5, self.face_5[8], self.direction_face_5]])
        self.proto_corner_cubes.append([[1, self.face_1[8], self.direction_face_1],
                                        [2, self.face_2[6], self.direction_face_2],
                                        [4, self.face_4[2], self.direction_face_4]])
        self.proto_corner_cubes.append([[3, self.face_3[0], self.direction_face_3],
                                        [5, self.face_5[0], self.direction_face_5],
                                        [6, self.face_6[2], self.direction_face_6]])
        self.proto_corner_cubes.append([[2, self.face_2[2], self.direction_face_2],
                                        [3, self.face_3[2], self.direction_face_3],
                                        [6, self.face_6[0], self.direction_face_6]])
        self.proto_corner_cubes.append([[4, self.face_4[6], self.direction_face_4],
                                        [5, self.face_5[6], self.direction_face_5],
                                        [6, self.face_6[8], self.direction_face_6]])
        self.proto_corner_cubes.append([[2, self.face_2[8], self.direction_face_2],
                                        [4, self.face_4[8], self.direction_face_4],
                                        [6, self.face_6[6], self.direction_face_6]])

        self.proto_edge_cubes = list()
        self.proto_edge_cubes.append([[1, self.face_1[1], self.direction_face_1],
                                      [3, self.face_3[7], self.direction_face_3]])
        self.proto_edge_cubes.append([[1, self.face_1[5], self.direction_face_1],
                                      [2, self.face_2[3], self.direction_face_2]])
        self.proto_edge_cubes.append([[1, self.face_1[7], self.direction_face_1],
                                      [4, self.face_4[1], self.direction_face_4]])
        self.proto_edge_cubes.append([[1, self.face_1[3], self.direction_face_1],
                                      [5, self.face_5[5], self.direction_face_5]])
        self.proto_edge_cubes.append([[3, self.face_3[3], self.direction_face_3],
                                      [5, self.face_5[1], self.direction_face_5]])
        self.proto_edge_cubes.append([[3, self.face_3[5], self.direction_face_3],
                                      [2, self.face_2[1], self.direction_face_2]])
        self.proto_edge_cubes.append([[2, self.face_2[7], self.direction_face_2],
                                      [4, self.face_4[5], self.direction_face_4]])
        self.proto_edge_cubes.append([[4, self.face_4[3], self.direction_face_4],
                                      [5, self.face_5[7], self.direction_face_5]])
        self.proto_edge_cubes.append([[3, self.face_3[1], self.direction_face_3],
                                      [6, self.face_6[1], self.direction_face_6]])
        self.proto_edge_cubes.append([[5, self.face_5[3], self.direction_face_5],
                                      [6, self.face_6[5], self.direction_face_6]])
        self.proto_edge_cubes.append([[4, self.face_4[7], self.direction_face_4],
                                      [6, self.face_6[7], self.direction_face_6]])
        self.proto_edge_cubes.append([[2, self.face_2[5], self.direction_face_2],
                                      [6, self.face_6[3], self.direction_face_6]])

        self.proto_center_cubes = list()
        self.proto_center_cubes.append([[1, self.face_1[4], self.direction_face_1]])
        self.proto_center_cubes.append([[2, self.face_2[4], self.direction_face_2]])
        self.proto_center_cubes.append([[3, self.face_3[4], self.direction_face_3]])
        self.proto_center_cubes.append([[4, self.face_4[4], self.direction_face_4]])
        self.proto_center_cubes.append([[5, self.face_5[4], self.direction_face_5]])
        self.proto_center_cubes.append([[6, self.face_6[4], self.direction_face_6]])
