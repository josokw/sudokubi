# Sudokubi

3D Sudoku solver: *Sudok*u + C*ubi* = *Sudokubi*

The Cubi series is a group of stainless steel sculptures built from cubes, rectangular solids and cylinders with
spheroidal or flat endcaps. These pieces are among the last works completed by the sculptor David Smith. Because a
Sudokubi consists of 27 small cubes, inspired us to use this name in the name of the developed solver.

This is **NOT** a *Sudoku Cube* puzzle based on Rubik's cube created in 2006 by Jay Horowitz. This solver solves the
wooden *Su Dokube* <sup>(c)</sup> puzzle from Gazebo Games International Ltd. You need to end up with each face of the "
kube" having one of each number showing from 1 to 9.

## Python

Requires Python version >= 3.5

## Design

The Sudokubi consists of 27 cubes.

The cubes are represented by 6 faces, the faces are numbered corresponding to regular dice values.

2D view of 6 faces:

```
        3-----+            
        |     |           
        |     |                                                                                                   
        +-----+                                       
5-----+ 1-----+ 2-----+ 6-----+
|     | |     | |     | |     |
|     | |     | |     | |     |
+-----+ +-----+ +-----+ +-----+
        4-----+                     
        |     |                   
        |     |                 
        +-----+
```

The cubes are one of four types, corresponding to the amount of values presented on them:

- 1 CoreCube
- 6 CenterCube's
- 12 EdgeCube's
- 8 CornerCube's

Some *Su Dokube* <sup>(c)</sup> cube examples:

![](./wooden_cubes.jpg)

The symbols 6 and 9 are identical under rotation of 180 degrees. The Sudokubi consists of 27 cubes (or cube_locations)
with unique coordinates x, y, z. Possible coordinates are 0, 1, or 2. The orientation of each face is:

- Face 1: xy-plane, z = 2
- Face 2: yz-plane, x = 2
- Face 3: xz-plane, y = 0
- Face 4: xz-plane, y = 2
- Face 5: yz-plane, x = 0
- Face 6: xy-plane, z = 0

## Running this program

Go to **src** directory:

```
python main.py -h
```

```bash
Sudokubi v1.0.0
usage: main.py [-h] [-v] [-m] {R} ...

Sudokubi solves a 3D Sudoku

optional arguments:
  -h, --help      show this help message and exit
  -v, --version   show version (default: False)
  -m, --metadata  show metadata (default: False)

R selection:
  {R}             options
    R             Random options
```

Help for the R option:

```bash
python main.py R -h
```

```bash
Sudokubi v1.0.0
usage: main.py R [-h] [-s SEED] [-a ATTEMPTS]

optional arguments:
  -h, --help            show this help message and exit
  -s SEED, --seed SEED  set seed <int> random generator
  -a ATTEMPTS, --attempts ATTEMPTS
                        set the number of attempts <int> >= 1
```

Go to **src** directory for solving 3 random generated sets of cubes:

```
python main.py R -s 10 -a 3
```

Go to **src** directory for solving the *Su Dokube* <sup>(c)</sup> puzzle:

```
python main.py
```

This will show 2 solutions.

## The Current Code and Future Ambitions

The current version of the code does what it set out to do: solve (and visualize its solution of) the Su Dokube puzzle.
It already does a bit more than that, as also explained above. The code is written with even more functions in mind,
which are not all completely worked out, something you might recognize in the code as well. This also means that this
code is by no means the most efficient code to do *what it is doing right now*. The code carries in it an ambition for
future developments - which is really something that will happen, but this version was to be published first.

The Symbol and SymbolManager classes form an important example: they give room for working with sets of 9 symbols other
than the numbers 1-9. The current code, however, doesn't do anything with this yet.

*I will be happy if a future version presents even a fraction of the features I am today dreaming of!*
